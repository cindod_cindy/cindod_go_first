package main

import "fmt"

func main(){
	name := "Cindod"
	counter := 0

	increment := func(){
		name := "Cindy"
		fmt.Println("Increment")
		counter++
		fmt.Println(name)
	}
	increment()
	increment()

	fmt.Println(counter)
	fmt.Println(name)
}