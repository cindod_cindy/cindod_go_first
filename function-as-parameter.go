package main

import "fmt"

func sayHelloWithFilter(name string, filter func(string) string){
	namedFiltered := filter(name)
	fmt.Println("Hello", namedFiltered)
}

func spamFilterName(name string) string{
	if name == "Anjing"{
		return "..."
	}else{
		return name
	}
	
}

func main(){
	sayHelloWithFilter("Cindod", spamFilterName)
	sayHelloWithFilter("Anjing", spamFilterName)
	sayHelloWithFilter2("Cindod", spamFilterName)
}

type Filter func(string) string

func sayHelloWithFilter2(name string, filter Filter){
	namedFiltered := filter(name)
	fmt.Println("Hello", namedFiltered)
}