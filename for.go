package main

import "fmt"

func main()  {

	counter := 1

	for counter <= 10{
		fmt.Println("Perulangan Ke ", counter)
		counter++
	}

	for counter2 := 1; counter2 < 10 ; counter2++{
		fmt.Println("Perulangan ke", counter2)
	}

	slice := []string {"Cindod", "Dot", "Cindy"}
	for i := 0 ; i< len(slice); i++{
		fmt.Println(slice[i])
	}
	for i, value := range slice {
		fmt.Println("Index", i, "=", value)
	}

	for _, value := range slice {
		//fmt.Println("Index", i, "=", value)
		fmt.Println(value)
	}

	person := make(map[string]string)
	person["name"] = "Cindod"
	person["tittle"] = "Junior Backend"

	for key, value := range person {
		fmt.Println(key, "=", value)
	}
	
}