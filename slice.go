package main

import "fmt"

func main()  {
	 var months = [...] string{
		 "Januari",
		 "Februari",
		 "Maret",
		 "April",
		 "Mei",
		 "Juni",
		 "Juli",
		 "Agustus",
		 "September",
		 "Oktober",
		 "November",
		 "Desember",
	 }

	 var slice1 = months[4:7]
	 fmt.Println(slice1)
	 fmt.Println(len(slice1))//3 krna isinya 3
	 fmt.Println(cap(slice1))//8 krna dari mei k dec ada 8

	 months[5]="Diubah"
	 fmt.Println(slice1)
	 months[4]="Mei Lagi"
	 fmt.Println(slice1)

	 var slice2 = months[10:]
	 fmt.Println(slice2)

	 var slice3 = append(slice2,"Cindod")
	 fmt.Println(slice3)

	 slice3[1]= "Bukan Desember"
	 fmt.Println(slice3)
	 fmt.Println(slice2)
	 fmt.Println(months)

	 newSlice := make([]string, 2,5)

	 newSlice[0] = "Cindod"
	 newSlice[1] = "Cindy"

	 fmt.Println(newSlice)

	 copySlice := make([]string, len(newSlice), cap(newSlice))
	 copy(copySlice, newSlice)
	 fmt.Println(copySlice)
	 copySliceByElement := make ([]string, 1, cap(newSlice))
	 fmt.Println(copySliceByElement)

	 lenSlice := make([]string, len(newSlice))
	 fmt.Println(lenSlice)//tapi ini kenapa kosong yha
	 capSlice :=make([]string, cap(newSlice))
	 fmt.Println(capSlice)

	 iniArray := [5] int {1,2,3,4,5}
	 iniSlice :=[] int {1,2,3,4,5}

	 fmt.Println("Ini Array",iniArray)
	 fmt.Println("Ini Slice",iniSlice)


	
}